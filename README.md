# Led screen thingy

## Building

### Desktop
SDL2 renderer should work out-of the box. Install libsdl-dev to build.

### Raspberry
Led renderer for raspberry is a bit hard to build. Your best bet
is to use containerized building with [cross](https://github.com/cross-rs/cross)
```shell
cross build --target armv7-unknown-linux-gnueabihf --release
```
build files will be written to target as usual.

### Windows
No

## Other stuff

Make sure lib/librgbmatrix.a exists before building