use std::fmt::{Display, Formatter};
use interpolation::Lerp;

#[derive(Copy, Clone)]
pub struct PixelColor {
    pub red: u8,
    pub green: u8,
    pub blue: u8
}

impl PixelColor {
    pub fn mul(&self, val: f32) -> PixelColor {
        PixelColor {
            red: (self.red as f32 * val) as u8,
            green: (self.green as f32 * val) as u8,
            blue: (self.blue as f32 * val) as u8,
        }
    }
    pub fn mix(&self, bg: &PixelColor, val: &f32) -> PixelColor {
        PixelColor {
            red: self.red.lerp(&bg.red, val),
            green: self.green.lerp(&bg.green, val),
            blue: self.blue.lerp(&bg.blue, val)
        }
    }
}

impl Display for PixelColor {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "[{}, {}, {}]", self.red, self.green, self.blue)
    }
}