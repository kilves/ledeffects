use std::cmp::max;
use fontdue::{Font, Metrics};
use crate::pixel_color::PixelColor;
use crate::target_canvas::{TargetBlending, TargetCanvas};

pub struct Character {
    metrics: Metrics,
    bitmap: Vec<u8>,
}

fn make_text(font: &Font, text: &str) -> Vec<Character> {
    let mut characters: Vec<_> = vec![];
    for c in text.chars() {
        let (metrics, bitmap) = font.rasterize(c, 8.0);
        characters.push(Character {
            metrics, bitmap
        })
    }
    characters
}

fn get_text_size(characters: &Vec<Character>) -> (usize, usize) {
    let text_width = characters
        .iter()
        .fold(0, |sum, c| sum + max(c.metrics.width, 4) + 1);
    let text_height = characters
        .iter()
        .fold(0, |sum, c| max(c.metrics.height, sum));
    (text_width, text_height)
}


pub fn draw_text_center(target: &mut TargetCanvas, font: &Font, text: &str, color: PixelColor, alpha: f32, offset_y: usize) {
    let text = make_text(font, text);
    let alpha_u8 = (alpha * 255.0) as u8;
    let (w, h) = target.get_size();
    let (text_width, text_height) = get_text_size(&text);
    let text_x =  w / 2 - text_width / 2;
    let mut offset = 0;
    for char in text {
        for y in 0..char.metrics.height {
            for x in 0..char.metrics.width {
                let tx = x + offset + char.metrics.bounds.xmin as usize;
                let ty = y + char.metrics.bounds.ymin as usize;
                let pixel = *char.bitmap.get(x + y * char.metrics.width).unwrap_or(&0);
                if pixel > 0 && tx < w {
                    let clr = color.mul((pixel as f32 / 255.0) as f32);
                    target.set(text_x + tx, h - text_height + ty - offset_y, &clr, &Some(TargetBlending::ALPHA(alpha_u8)));
                }
            }
        }
        offset += max(char.metrics.width, 4) + 1;
    }
}