use std::cmp::max;
use std::fmt::{Display, Formatter};
use cgmath::{Matrix4, Point3, Transform};
use image::Pixel;
use crate::pixel_color::PixelColor;

pub enum TargetBlending {
    ADDITIVE,
    SCREEN,
    BRIGHTER,
    ALPHA(u8)
}

impl Display for TargetBlending {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            TargetBlending::ADDITIVE => "Additive",
            TargetBlending::SCREEN => "Screen",
            TargetBlending::BRIGHTER => "Brighter",
            TargetBlending::ALPHA(_) => "Alpha"
        })
    }
}

pub struct TargetCanvas {
    width: usize,
    height: usize,
    data: Vec<u8>,
}

fn blend_additive_f(a: f32, b: f32) -> f32 {
    (a + b).min(255.0)
}

fn blend_additive(a: u8, b: u8) -> u8 {
    blend_additive_f(a as f32, b as f32) as u8
}

fn blend_alpha(a: u8, b: u8, al: &u8) -> u8 {
    let alpha = *al as f32 / 255.0;
    let bf = b as f32;
    let af = a as f32;

    (alpha * af + (1.0 - alpha) * bf) as u8
}

fn blend_screen(a: u8, b: u8) -> u8 {
    let af = a as f32 / 255.0;
    let bf = b as f32 / 255.0;
    ((1.0 - (1.0 - af) * (1.0 - bf)) * 255.0) as u8
}

pub fn blend_alpha_color(a: PixelColor, b: PixelColor, blend: f32) -> PixelColor {
    let alpha = (blend * 255.0).floor() as u8;
    PixelColor {
        red: blend_alpha(a.red, b.red, &alpha),
        blue: blend_alpha(a.blue, b.blue, &alpha),
        green: blend_alpha(a.green, b.green, &alpha)
    }
}

impl TargetCanvas {

    pub fn new(width: usize, height: usize) -> TargetCanvas {
        TargetCanvas {
            width,
            height,
            data: vec![0; width * height * 3],
        }
    }

    pub fn project_3d(&self, point: &Point3<f32>, mvp: &Matrix4<f32>) -> (i32, i32) {
        let pos = mvp.transform_point(*point);
        self.normalized_to_absolute(pos.x, pos.y)
    }

    pub fn normalized_to_absolute(&self, x: f32, y: f32) -> (i32, i32) {
        let xf = (x + 1.0) / 2.0;
        let yf = (y + 1.0) / 2.0;
        let wf = self.width as f32;
        let hf = self.height as f32;
        ((xf * wf) as i32, (yf * hf) as i32)
    }

    pub fn set(&mut self, x: usize, y: usize, color: &PixelColor, blending: &Option<TargetBlending>) {
        if x >= self.width || y >= self.height || x < 0 || y < 0 {
            return;
        }
        let point = (self.width * y + x) * 3;
        let previous_color = PixelColor {
            red: self.data[point],
            green: self.data[point + 1],
            blue: self.data[point + 2]
        };
        match blending {
            None => {
                self.data[point] = color.red;
                self.data[point + 1] = color.green;
                self.data[point + 2] = color.blue;
            }
            Some(TargetBlending::ADDITIVE) => {
                self.data[point] = blend_additive(color.red, previous_color.red);
                self.data[point + 1] = blend_additive(color.green, previous_color.green);
                self.data[point + 2] = blend_additive(color.blue, previous_color.blue);
            }
            Some(TargetBlending::ALPHA(value)) => {
                self.data[point] = blend_alpha(color.red, previous_color.red, value);
                self.data[point + 1] = blend_alpha(color.green, previous_color.green, value);
                self.data[point + 2] = blend_alpha(color.blue, previous_color.blue, value);
            },
            Some(TargetBlending::BRIGHTER) => {
                let b1 = max(max(color.red, color.blue), color.green);
                let b2 = max(max(previous_color.red, previous_color.blue), previous_color.green);
                self.data[point] = if b1 > b2 {color.red} else {previous_color.red};
                self.data[point + 1] = if b1 > b2 {color.green} else {previous_color.green};
                self.data[point + 2] = if b1 > b2 {color.blue} else {previous_color.blue};
            },
            Some(TargetBlending::SCREEN) => {
                self.data[point] = blend_screen(color.red, previous_color.red);
                self.data[point + 1] = blend_screen(color.green, previous_color.green);
                self.data[point + 2] = blend_screen(color.blue, previous_color.blue);
            }
        }
    }

    pub fn get(&self, x: usize, y: usize) -> PixelColor {
        let point = (self.width * y + x) * 3;
        PixelColor {
            red: self.data[point],
            green: self.data[point + 1],
            blue: self.data[point + 2]
        }
    }

    pub fn blend(&mut self, c1: &TargetCanvas, c2: &TargetCanvas, blend: f32) {
        let size = self.get_size();
        let w = size.0;
        let h = size.1;
        for y in 0..h {
            for x in 0..w {
                let p1 = c1.get(x, y);
                let p2 = c2.get(x, y);
                self.set(x, y, &blend_alpha_color(p1, p2, blend), &None);
            }
        }
    }

    pub fn clear(&mut self) {
        self.data.fill(0)
    }

    pub fn get_size(&self) -> (usize, usize) {
        (self.width, self.height)
    }

    pub fn get_size_u32(&self) -> (u32, u32) {
        (self.width as u32, self.height as u32)
    }

    pub fn get_size_i32(&self) -> (i32, i32) {
        (self.width as i32, self.height as i32)
    }
}