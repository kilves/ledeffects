use rpi_led_matrix::{LedMatrix, LedMatrixOptions, LedRuntimeOptions, LedColor, LedCanvas};
use crate::renderers::renderer::Renderer;
use crate::pixel_render_target::PixelRenderTarget;
use std::borrow::BorrowMut;
use noise::Perlin;
use crate::util::{cf, ScaledTrig};
use crate::pixel_color::PixelColor;
use image::Pixel;

pub struct PlatformRenderer {
    led_matrix: LedMatrix,
    target: PixelRenderTarget,
    offscreen_canvas: Option<LedCanvas>,
}

impl Renderer for PlatformRenderer {
    fn new(width: usize, height: usize) -> PlatformRenderer {
        Self::new(width, height)
    }

    fn set(&mut self, x: i32, y: i32, color: &PixelColor) {
        self.offscreen_canvas.as_mut().unwrap().set(x, y, &LedColor {
            red: color.red,
            blue: color.blue,
            green: color.green
        });
    }

    fn clear(&mut self) {
        self.offscreen_canvas.as_mut().unwrap().clear();
    }

    fn swap(&mut self, paused: bool) -> Result<(), ()> {
        let (w, h) = self.get_size();
        if (!paused) {
            for x in 0..w {
                for y in 0..h {
                    let color = self.target.get(x as usize, y as usize);
                    self.set(x as i32, y as i32, &color)
                }
            }
        }
        let canvas = self.offscreen_canvas.take().unwrap();
        if (paused) {
            Some(self.led_matrix.swap(canvas));
            self.offscreen_canvas = Some(self.led_matrix.canvas());
        } else {
            self.offscreen_canvas = Some(self.led_matrix.swap(canvas));
        }

        Ok(())
    }

    fn fill(&mut self, color: &PixelColor) -> Result<(), String> {
        let mut canvas = self.offscreen_canvas.as_mut().unwrap();
        Ok(canvas.fill(&LedColor {
            red: color.red,
            green: color.green,
            blue: color.blue
        }))
    }

    fn get_target(&mut self) -> &mut PixelRenderTarget {
        self.target.borrow_mut()
    }

    fn get_size(&self) -> (usize, usize) {
        let size = self.led_matrix.canvas().canvas_size();
        (size.0 as usize, size.1 as usize)
    }
}

impl PlatformRenderer {

    fn new(width: usize, height: usize) -> PlatformRenderer {
        let mut matrix_options = LedMatrixOptions::new();
        matrix_options.set_hardware_mapping("adafruit-hat");
        matrix_options.set_rows(height as u32);
        matrix_options.set_cols(width as u32);
        matrix_options.set_pwm_lsb_nanoseconds(150);
        let mut runtime_options = LedRuntimeOptions::new();
        runtime_options.set_gpio_slowdown(2);
        let led_matrix = LedMatrix::new(
            Some(matrix_options),
            Some(runtime_options)
        ).unwrap();
        let offscreen_canvas = led_matrix.offscreen_canvas();
        PlatformRenderer {
            led_matrix,
            target: PixelRenderTarget::new(width, height),
            offscreen_canvas: Some(offscreen_canvas),
        }
    }
}