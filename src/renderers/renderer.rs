use crate::pixel_color::PixelColor;
use crate::pixel_render_target::PixelRenderTarget;

pub trait Renderer {
    fn new(width: usize, height: usize) -> Self;
    fn set(&mut self, x: i32, y: i32, color: &PixelColor);
    fn clear(&mut self);
    fn swap(&mut self, paused: bool) -> Result<(), ()>;
    fn fill(&mut self, color: &PixelColor) -> Result<(), String>;

    fn get_target(&mut self) -> &mut PixelRenderTarget;

    fn get_size(&self) -> (usize, usize);
}