pub mod renderer;
#[cfg_attr(target_arch = "arm", path = "led_renderer.rs")]
#[cfg_attr(target_arch = "x86_64", path = "sdl_renderer.rs")]
pub mod platform_renderer;