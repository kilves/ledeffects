use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::render::{WindowCanvas};
use sdl2::EventPump;
use crate::renderers::renderer::Renderer;
use crate::pixel_color::PixelColor;
use std::borrow::BorrowMut;
use cgmath::Vector2;
use crate::pixel_render_target::PixelRenderTarget;
use crate::util::c;

pub struct PlatformRenderer {
    canvas: WindowCanvas,
    event_pump: EventPump,
    size: (usize, usize),
    pixel_size: usize,
    target: PixelRenderTarget,
}

impl Renderer for PlatformRenderer {
    fn new(width: usize, height: usize) -> Self {
        Self::new(width, height, 10)
    }

    fn set(&mut self, x: i32, y: i32, color: &PixelColor) {
        self.canvas.set_draw_color(Color::RGB(color.red, color.green, color.blue));
        self.canvas.fill_rect(Rect::new(
            x * self.pixel_size as i32,
            y * self.pixel_size as i32,
            self.pixel_size as u32,
            self.pixel_size as u32
        )).unwrap();
    }

    fn clear(&mut self) {
        self.canvas.set_draw_color(Color::RGB(0, 0, 0));
        self.canvas.clear();
    }

    fn swap(&mut self, paused: bool) -> Result<(), ()> {
        let (w, h) = self.get_size();
        if !paused {
            for x in 0..w {
                for y in 0..h {
                    let color = self.target.get(x as usize, y as usize);
                    self.set(x as i32, y as i32, &color)
                }
            }
        }
        self.canvas.present();
        for event in self.event_pump.poll_iter() {
            match event  {
                Event::Quit { .. } |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    return Err(());
                },
                _ => {}
            }
        }
        Ok(())
    }

    fn fill(&mut self, color: &PixelColor) -> Result<(), String> {
        self.canvas.set_draw_color(Color::RGB(color.red, color.green, color.blue));
        self.canvas.fill_rect(Rect::new(
            0,
            0,
            (self.size.0 * self.pixel_size) as u32,
            (self.size.1 * self.pixel_size) as u32
        ))
    }

    fn get_target(&mut self) -> &mut PixelRenderTarget {
        self.target.borrow_mut()
    }

    fn get_size(&self) -> (usize, usize) {
        self.size
    }
}

impl PlatformRenderer {

    fn new(width: usize, height: usize, pixel_size: usize) -> Self {
        let sdl_context = sdl2::init().unwrap();
        let video = sdl_context.video().unwrap();

        let window = video.window(
            "led matrix",
            (width * pixel_size) as u32,
            (height * pixel_size) as u32
        )
            .position_centered()
            .build()
            .unwrap();
        PlatformRenderer {
            canvas: window.into_canvas().build().unwrap(),
            event_pump: sdl_context.event_pump().unwrap(),
            size: (width, height),
            pixel_size,
            target: PixelRenderTarget::new(width, height)
        }
    }
}