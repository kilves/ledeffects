use crate::pixel_color::PixelColor;
use crate::target_canvas::{blend_alpha_color, TargetCanvas};

pub struct PixelRenderTarget {
    width: usize,
    height: usize,
    pub primary: TargetCanvas,
    pub secondary: TargetCanvas,
    result: TargetCanvas,
}

impl PixelRenderTarget {
    pub fn new(width: usize, height: usize) -> PixelRenderTarget {
        PixelRenderTarget {
            width, height,
            primary: TargetCanvas::new(width, height),
            secondary: TargetCanvas::new(width, height),
            result: TargetCanvas::new(width, height)
        }
    }

    pub fn draw_result(&mut self, blend: f32) {
        for y in 0..self.height {
            for x in 0..self.width {
                self.result.set(x, y, &blend_alpha_color(self.primary.get(x, y), self.secondary.get(x, y), blend), &None);
            }
        }
    }

    pub fn get(&self, x: usize, y: usize) -> PixelColor {
        self.result.get(x, y)
    }

    pub fn clear(&mut self) {
        self.primary.clear();
        self.secondary.clear();
        self.result.clear();
    }
}