use std::cmp::max;
use std::ops::{Add, Mul};
use cgmath::{Deg, Matrix4, Point3, Rad, Vector3};
use chrono::{Duration, Local, TimeZone};
use fontdue::{Font, Metrics};
use crate::pixel_color::PixelColor;
use crate::target_canvas::{TargetBlending, TargetCanvas};

pub trait ScaledTrig {
    fn scaled_sin(&mut self) -> Self;
    fn scaled_cos(&mut self) -> Self;
    fn as_scaled_sin(&self) -> Self;
    fn as_scaled_cos(&self) -> Self;
}

impl ScaledTrig for f32 {
    fn scaled_sin(&mut self) -> f32 {
        self.sin().add(1.0).mul(0.5)
    }

    fn scaled_cos(&mut self) -> f32 {
        self.cos().add(1.0).mul(0.5)
    }

    fn as_scaled_sin(&self) -> Self {
        self.sin().add(1.0).mul(0.5)
    }

    fn as_scaled_cos(&self) -> Self {
        self.cos().add(1.0).mul(0.5)
    }
}

pub fn time_to_new_year() -> Duration {
    let now = Local::now();
    let duration = Local.with_ymd_and_hms(2024, 1, 1, 0, 0, 0)
        .unwrap()
        .signed_duration_since(now);
    if duration.num_seconds() <= 0 {
        return Duration::seconds(0);
    }
    duration
}

pub fn c(r: u8, g: u8, b: u8) -> PixelColor {
    PixelColor {
        red: r,
        green: g,
        blue: b
    }
}

pub fn cf(r: f32, g: f32, b: f32) -> PixelColor {
    PixelColor {
        red: (r * 255.0).round() as u8,
        green: (g * 255.0).round() as u8,
        blue: (b * 255.0).round() as u8,
    }
}

pub fn mvp_matrix(x: Rad<f32>, y: Rad<f32>, xoffset: f32, zoom: f32) -> Matrix4<f32> {
    let x_mat = Matrix4::from_angle_x(x);
    let y_mat = Matrix4::from_angle_y(y);
    let model_mat = x_mat * y_mat;
    let view_mat = Matrix4::look_at_rh(
        Point3::new(0.0, zoom, zoom),
        Point3::new(xoffset * 2.0, 0.0, 0.0),
        Vector3::unit_z()
    );
    let proj_mat = cgmath::perspective(Deg(60.0f32), 2.0, 0.1, 1000.0);
    proj_mat * view_mat * model_mat
}