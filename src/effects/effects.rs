use crate::effects::effect::Effect;
use std::time::Duration;
use crate::pixel_render_target::PixelRenderTarget;
use crate::target_canvas::TargetCanvas;

pub struct EffectConfig {
    pub interval: Duration,
    pub fade: Duration,
    pub size: (usize, usize)
}

pub struct Effects {
    effects: Vec<Box<dyn Effect>>,
    interval: Duration,
    fade: Duration,
    current_index: f32,
    current_canvas: TargetCanvas,
    next_canvas: TargetCanvas
}


impl Effects {
    pub fn new(config: EffectConfig) -> Effects {
        Effects {
            effects: vec![],
            interval: config.interval,
            fade: config.fade,
            current_index: 0.0,
            current_canvas: TargetCanvas::new(config.size.0, config.size.1),
            next_canvas: TargetCanvas::new(config.size.0, config.size.1)
        }
    }

    fn update(&mut self, c_secs: f32) {
        let i_secs = self.interval.as_secs_f32() + self.fade.as_secs_f32();
        self.current_index = (c_secs % (i_secs * self.effects.len() as f32)) / i_secs;
    }

    pub fn draw(&mut self, target: &mut PixelRenderTarget, duration: f32, secondary_timer: f32) -> f32 {
        self.update(duration);
        let blend = self.get_transition_blend();
        self.exec(target, secondary_timer);
        target.draw_result(1.0 - blend);
        self.current_index
    }

    pub fn override_newyear(&mut self, target: &mut PixelRenderTarget, duration: f32) {
        let mut my_effect = self.effects.iter_mut().find(|e| e.name() == "NEWYEAR")
            .unwrap().as_mut();
        my_effect.exec(&mut target.primary, duration);
        target.draw_result(1.0)
    }

    fn get_transition_blend(&self) -> f32 {
        let ratio = 1.0 - self.fade.as_secs_f32() / self.interval.as_secs_f32();
        let curr = self.current_index - self.current_index.floor();
        ((curr - ratio) / (1.0 - ratio)).clamp(0.0, 1.0)
    }

    fn get(&mut self) -> &mut dyn Effect {
        let current_index = self.current_index.floor() as usize;
        self.effects[current_index].as_mut()
    }

    fn exec(&mut self, target: &mut PixelRenderTarget, duration: f32) {
        self.get().exec(&mut target.primary, duration);
        self.get_next().exec(&mut target.secondary, duration);
    }

    fn get_next(&mut self) -> &mut dyn Effect {
        let current_index = self.current_index.floor() as usize;
        if current_index == self.effects.len() - 1 {
            self.effects[0].as_mut()
        } else {
            self.effects[current_index + 1].as_mut()
        }
    }

    pub fn put<T: 'static + Effect>(&mut self, effect: T) {
        self.effects.push(Box::new(effect));
    }

    fn clear(&mut self) {
        self.effects.clear()
    }

    pub fn len(&self) -> usize {
        self.effects.len()
    }

}
