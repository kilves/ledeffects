use crate::effects::effect::Effect;
use crate::target_canvas::TargetBlending::ALPHA;
use crate::target_canvas::TargetCanvas;
use crate::util::c;

pub struct XorEffect {
    turn: i64,
    path: Vec<f64>,
    count: i32
}

impl Effect for XorEffect {
    fn exec(&mut self, target: &mut TargetCanvas, duration: f32) {
        let (wu, hu) = target.get_size();
        let w = wu as f64;
        let h = hu as f64;
        let now = duration * 0.3;
        let nowfast = duration * 20.0;
        let duration1 = now * 0.1;
        let cr1: f64 = now.sin() as f64;
        let sr1: f64 = now.cos() as f64;
        let sr2: f64 = (duration1.cos() + duration1.sin()) as f64;
        for i in 0..hu {
            for j in 0..wu {
                let i_f = i as f64;
                let j_f = j as f64;
                let mut a: f64 = j_f - w * sr1;
                let mut b: f64 = i_f - h * cr1;
                let x = 8.0 + 8.0 * ((a * a + b * b).sqrt() * 0.1).sin();
                a = j_f - w / 2.0;
                b = i_f - h / 2.0;
                let y = 8.0 + 8.0 * ((a * a + b * b).sqrt() * 0.1).sin();

                let ix = j;
                let iy = i;
                let d = (x as i64).wrapping_pow(y as u32);
                let bytes = (d << (4 + (nowfast % 12.0 as f32) as usize)).to_ne_bytes();
                target.set(ix, iy, &c(bytes[0], bytes[1], bytes[2]), &Some(ALPHA(bytes[3])));
            }
        }
    }

    fn name(&self) -> &'static str {
        "XOR"
    }
}

impl XorEffect {
    pub fn new() -> XorEffect {
        let mut effect = XorEffect {
            turn: 0,
            path: Vec::with_capacity(1024),
            count: 0
        };
        for i in 0..1024 {
            let val: f64 = (((i as f32 * 0.3515625) * 0.0174532) * 10.0) as f64;
            effect.path.push(val);
        }
        effect
    }
}