use crate::effects::effect::Effect;
use crate::target_canvas::{TargetCanvas, TargetBlending};
use crate::util::{cf};
use noise::Perlin;
use noise::NoiseFn;

fn meta_diamond(px: f32, py: f32, pix: f32, piy: f32, r: f32) -> f32 {
    let dx = (px - pix).abs();
    let dy = (py - piy).abs();
    let clamp_val = r / (dx.sqrt() + dy.sqrt()).powf(2.0);
    let light_val = r / (dx + dy);
    if clamp_val > 0.1 { light_val } else { 0.0 }
}

pub struct MetaEffect;

impl Effect for MetaEffect {
    fn exec(&mut self, target: &mut TargetCanvas, duration: f32) {
        let perlin = Perlin::new();
        let (w, h) = target.get_size();
        let wf = w as f32;
        let hf = h as f32;
        let now = duration * 3.0;
        let flicker = duration as f64 * 15.0;
        target.set(0, 0, &cf(1.0, 0.0, 0.0), &None);
        for x in 0..w {
            for y in 0..h {
                let xf = x as f32;
                let yf = y as f32;
                let d1 = meta_diamond(
                    w as f32 / 2.0 + now.sin() * 3.6,
                    h as f32 / 2.0 + now.cos() * 4.3,
                    xf + (1.0 * now).cos() * wf / 4.0,
                    yf + (1.0 * now).sin() * hf / 4.0,
                    0.5 + now.sin() * 0.5 + perlin.get([flicker, flicker]) as f32 * 1.0
                );

                let d2 = meta_diamond(
                    w as f32 / 2.0 - now.cos() * 3.6,
                    h as f32 / 2.0 - now.sin() * 4.3,
                    xf - (1.0 * now).cos() * wf / 4.0,
                    yf - (1.0 * now).sin() * hf / 4.0,
                    1.4 + now.sin() * 0.5 + perlin.get([flicker + 1000.0, flicker]) as f32 * 1.0
                );
                let now_r = -now;
                let d3 = meta_diamond(
                    w as f32 / 2.0,
                    h as f32 / 2.0,
                    xf - (1.0 * now_r).cos() * wf / 4.0,
                    yf - (1.0 * now_r).sin() * hf / 4.0,
                    1.0 + now.sin() * 0.5 + perlin.get([flicker + 1500.0, flicker]) as f32 * 1.0
                );

                target.set(x, y, &cf(d1, 0.0, 0.0), &None);
                target.set(x, y, &cf(0.0, d2, 0.0), &Some(TargetBlending::SCREEN));
                target.set(x, y, &cf(0.0, 0.0, d3), &Some(TargetBlending::SCREEN));
            }
        }
    }

    fn name(&self) -> &'static str {
        "Meta effect"
    }
}