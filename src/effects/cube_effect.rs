use std::f32::consts::PI;
use cgmath::{Deg, Matrix4, Point3, Rad, SquareMatrix, Transform, Vector3};
use rusttype::Point;
use crate::effects::drawutils::draw_line;
use crate::effects::effect::Effect;
use crate::effects::imageutils::draw_image;
use crate::target_canvas::{TargetBlending, TargetCanvas};
use crate::util::{cf, mvp_matrix, ScaledTrig};

pub struct CubeEffect {
    lines: Vec<(Point3<f32>, Point3<f32>)>,
}

impl Effect for CubeEffect {

    fn exec(&mut self, target: &mut TargetCanvas, duration: f32) {

        let mvp = mvp_matrix(Rad(duration), Rad(duration * 2.0), 0.0, 2.0);
        for line in &self.lines {
            let (x1, y1) = target.project_3d(&line.0, &mvp);
            let (x2, y2) = target.project_3d(&line.1, &mvp);
            draw_line(target, x1, y1, x2, y2, &cf((duration * 5.5).scaled_sin(), (duration * 100.3).scaled_cos(), (duration * 1.3).scaled_cos()), &None);
        }
    }

    fn name(&self) -> &'static str {
        "Cube effect"
    }
}


impl CubeEffect {

    pub fn new() -> CubeEffect {
        let eye = Point3::new(0.0, 0.0, 4.0);
        let target = Point3::new(0.0, 0.0, 0.0);
        CubeEffect {
            lines: vec![
                (Point3::new(1.0, 1.0, 1.0), Point3::new(-1.0, 1.0, 1.0)),
                (Point3::new(-1.0, 1.0, 1.0), Point3::new(-1.0, 1.0, -1.0)),
                (Point3::new(-1.0, 1.0, -1.0), Point3::new(1.0, 1.0, -1.0)),
                (Point3::new(1.0, 1.0, -1.0), Point3::new(1.0, 1.0, 1.0)),

                (Point3::new(1.0, -1.0, 1.0), Point3::new(-1.0, -1.0, 1.0)),
                (Point3::new(-1.0, -1.0, 1.0), Point3::new(-1.0, -1.0, -1.0)),
                (Point3::new(-1.0, -1.0, -1.0), Point3::new(1.0, -1.0, -1.0)),
                (Point3::new(1.0, -1.0, -1.0), Point3::new(1.0, -1.0, 1.0)),

                (Point3::new(1.0, 1.0, 1.0), Point3::new(1.0, -1.0, 1.0)),
                (Point3::new(-1.0, 1.0, 1.0), Point3::new(-1.0, -1.0, 1.0)),
                (Point3::new(-1.0, 1.0, -1.0), Point3::new(-1.0, -1.0, -1.0)),
                (Point3::new(1.0, 1.0, -1.0), Point3::new(1.0, -1.0, -1.0)),
            ]
        }
    }
}