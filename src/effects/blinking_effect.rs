use std::f32::consts::PI;
use std::ops::Mul;
use crate::effects::effect::Effect;
use crate::target_canvas::TargetCanvas;
use rand::rngs::SmallRng;
use rand::{SeedableRng, Rng};
use crate::effects::drawutils::draw_line;
use crate::util::{cf, ScaledTrig};
use crate::pixel_color::PixelColor;
use crate::target_canvas::TargetBlending::ALPHA;

pub struct BlinkingEffect;

fn draw_star(target: &mut TargetCanvas, x: i32, y: i32, color: &PixelColor) {
    let blending = Some(ALPHA(80));

    // draw_line(target, x, y - 2, x, y + 2, color, &blending);
    // draw_line(target, x + 2, y, x - 2, y, color, &blending);

    draw_line(target, x, y - 3, x, y - 2, color, &blending);
    draw_line(target, x, y + 3, x, y + 2, color, &blending);
    draw_line(target, x - 3, y, x - 2, y, color, &blending);
    draw_line(target, x + 3, y, x + 2, y, color, &blending);

    draw_line(target, x - 2, y - 2, x + 2, y + 2, color, &blending);
    draw_line(target, x + 2, y - 2, x - 2, y + 2, color, &blending);

    draw_line(target, x - 1, y - 1, x + 1, y + 1, color, &blending);
    draw_line(target, x + 1, y - 1, x - 1, y + 1, color, &blending);
}
fn stars(target: &mut TargetCanvas, intensity: f32, seed: u64, amount: usize) {
    const MARGIN: i32 = 3;
    let mut light_rng = SmallRng::seed_from_u64(seed);
    let (w, h) = target.get_size();
    for _ in 0..amount {
        let x = light_rng.gen_range(MARGIN..(w as i32 - MARGIN));
        let y = light_rng.gen_range(MARGIN..(h as i32 - MARGIN));
        draw_star(target, x, y, &cf(
            intensity * light_rng.gen_range(0.0..1.0),
            intensity * light_rng.gen_range(0.0..1.0),
            intensity * light_rng.gen_range(0.0..1.0)
        ));
    }
}

const STAR_SETS: usize = 4;
const STARS_PER_SET: usize = 3;
const EFFECT_SPEED: f32 = 1.5;

impl Effect for BlinkingEffect {
    fn exec(&mut self, target: &mut TargetCanvas, duration: f32) {
        for x in 0..STAR_SETS {
            let ledsf = STAR_SETS as f32;
            let set_time = x as f32 / ledsf;
            let delta = (duration + set_time).mul(EFFECT_SPEED * PI);
            let seed = delta.floor();
            let intensity = delta - seed;
            let newseed = seed + x as f32 * seed;
            stars(target, 1.0 - intensity, newseed as u64, STARS_PER_SET);
        }
    }

    fn name(&self) -> &'static str {
        "Blinking effect"
    }
}