use std::f32::consts::PI;
use rand::{Rng, thread_rng};
use rand::prelude::IteratorRandom;
use crate::Effects;
use crate::effects::effect::Effect;
use crate::pixel_color::PixelColor;
use crate::target_canvas::TargetCanvas;
use crate::util::{c, cf};

pub enum FireworkStyle {
    DIZZLE
}

pub struct Firework {
    state: f32,
    duration: f32,
    secondary_duration: f32,
    x: f32,
    y: f32,
    ax: f32,
    ay: f32,
    color: PixelColor,
    style: Option<FireworkStyle>,
}

const GRAVITY: f32 = -4.5;
const MIN_INTERVAL: f32 = 0.05;
const MAX_INTERVAL: f32 = 0.3;
const BLINK_FREQ: f32 = 0.066;

fn get_pixel_position(x: f32, y: f32, target: &TargetCanvas) -> (usize, usize) {
    let (w, h) = target.get_size();
    let wf = w as f32;
    let hf = h as f32;
    (
        (x * wf / 2.0 + wf / 2.0) as usize,
        h.saturating_sub((y * hf) as usize),
    )
}

impl Firework {
    pub fn has_exploded(&self) -> bool {
        self.state > self.duration
    }
    pub fn has_expired(&self) -> bool {
        self.state > self.duration + self.secondary_duration
    }
    pub fn get_pixel_position(&self, target: &TargetCanvas) -> (usize, usize) {
        get_pixel_position(self.x, self.y, target)
    }
    pub fn update(&mut self, delta: f32) {
        self.state += delta;
        if !self.has_exploded() {
            self.x += self.ax * delta;
            self.y += self.ay * delta;
        }
        self.ay += GRAVITY * delta;
    }
    pub fn get_explosion_state(&self) -> f32 {
        if !self.has_exploded() { return 0.0; }
        (self.state - self.duration) / self.secondary_duration
    }
    fn draw_explosion(&mut self, target: &mut TargetCanvas, blink: bool) {
        match self.style {
            None => {
                let (px, py) = self.get_pixel_position(target);

                target.set(px, py, &cf(0.0, 1.0, 0.0), &None);
            }
            Some(FireworkStyle::DIZZLE) => {
                let count = 10;
                for point in 0..count {
                    let angle = point as f32 / count as f32 * PI * 2.0;
                    let x = angle.cos() * 0.3 * self.get_explosion_state() + self.x;
                    let y = angle.sin() * 0.3 * self.get_explosion_state() + self.y;
                    let (px, py) = get_pixel_position(x, y, target);
                    if blink {
                        target.set(px, py, &cf(1.0, 1.0, 1.0), &None);
                    } else {
                        target.set(px, py, &self.color, &None);

                    }
                }
            }
        }
    }
}

pub struct FireworksEffect {
    fireworks: Vec<Firework>,
    previous: f32,
    last_launched: f32,
    firework_interval: f32,
    last_blinked: f32,
}

impl Effect for FireworksEffect {

    fn exec(&mut self, target: &mut TargetCanvas, duration: f32) {
        let blink_white = duration> self.last_blinked + BLINK_FREQ;

        let delta = if self.previous == 0.0 { 0.0 } else { duration - self.previous };
        if duration > self.last_launched + self.firework_interval {
            self.launch_firework();
            self.last_launched = duration;
            self.firework_interval = thread_rng().gen_range(MIN_INTERVAL..MAX_INTERVAL);
        }
        for mut firework in self.fireworks.iter_mut() {
            firework.update(delta);
            let (w, h) = target.get_size();
            let (px, py) = firework.get_pixel_position(target);
            if firework.has_exploded() {
                firework.draw_explosion(target, blink_white);
            } else {
                target.set(px, py, &c(217, 103, 51), &None)
            }
        }
        self.cleanup();
        if blink_white {
            self.last_blinked = duration;
        }
        self.previous = duration;
    }

    fn name(&self) -> &'static str {
        "Fireworks"
    }
}

impl FireworksEffect {
    pub fn new() -> FireworksEffect {
        FireworksEffect {
            fireworks: vec![],
            previous: 0.0,
            last_launched: 0.0,
            firework_interval: 0.35,
            last_blinked: 0.0,
        }
    }

    fn cleanup(&mut self) {
        self.fireworks.retain(|fw| !fw.has_expired())
    }

    fn launch_firework(&mut self) {
        let mut rng = thread_rng();
        let colors: Vec<PixelColor> = vec![
            cf(1.0, 0.0, 0.0),
            cf(0.0, 1.0, 0.0),
            cf(0.0, 0.0, 1.0),
            c(246, 152, 53),
            c(255, 105, 180),
            c(18, 15, 82),
            c(142, 249, 61),
            c(255, 233, 37),
        ];
        self.fireworks.push(Firework {
            duration: 0.7,
            secondary_duration: 0.5,
            state: 0.0,
            x: rng.gen_range(-0.4..0.4),
            y: 0.0,
            ax: rng.gen_range(-0.5..0.5),
            color: colors[rng.gen_range(0..colors.len())],
            ay: 2.5,
            style: Some(FireworkStyle::DIZZLE)
        })
    }
}