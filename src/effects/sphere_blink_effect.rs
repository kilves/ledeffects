use crate::effects::effect::Effect;
use crate::target_canvas::{TargetCanvas, TargetBlending};
use rand::{thread_rng, Rng, SeedableRng};
use rand::rngs::SmallRng;
use crate::util::{cf};

pub struct SphereBlinkEffect {
    times: Vec<f32>
}

fn gen_times() -> Vec<f32> {
    let mut times = vec![];
    let mut rng = thread_rng();
    rng.gen_range(0.0..1.0);

    let mut time = 0.0;
    for _ in 0..1000 {
        time += rng.gen_range(0.0..0.2);
        times.push(time);
    }
    times
}

impl Effect for SphereBlinkEffect {
    fn exec(&mut self, target: &mut TargetCanvas, duration: f32) {
        let (w, h) = target.get_size();
        let max_time = self.times.last().unwrap() + 1.0;
        let time_point = duration % max_time;
        let period = (duration / max_time).floor();
        for time in &self.times {
            let seed = time + period;
            let mut sphere_rng = SmallRng::seed_from_u64(seed.to_bits() as u64);
            let pos_x = sphere_rng.gen_range(0.0..w as f32);
            let pos_y = sphere_rng.gen_range(0.0..h as f32);
            let red = sphere_rng.gen_range(0.0..1.0);
            let green = sphere_rng.gen_range(0.0..1.0);
            let blue = sphere_rng.gen_range(0.0..1.0);
            let radius = sphere_rng.gen_range(2.5..10.0);
            if (time - time_point).abs() > 1.0 {
                continue;
            }
            for x in 0..w {
                for y in 0..h {
                    let xf = x as f32;
                    let yf = y as f32;
                    let distance = ((xf - pos_x).powi(2) + (yf - pos_y).powi(2)).sqrt();
                    let mul = (1.0 - (distance / radius).min(1.0)) * (1.0 - ((time - time_point) * 5.0).abs().sqrt().min(1.0));
                    let color = cf(
                        red * mul,
                        green * mul,
                        blue * mul
                    );
                    target.set(x, y, &color, &Some(TargetBlending::ADDITIVE));
                }
            }
        }
    }

    fn name(&self) -> &'static str {
        "Sphere blink effect"
    }
}

impl SphereBlinkEffect {
    pub fn new() -> SphereBlinkEffect {
        SphereBlinkEffect {
            times: gen_times()
        }
    }
}