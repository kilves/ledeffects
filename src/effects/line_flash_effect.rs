use crate::effects::effect::Effect;
use crate::target_canvas::TargetCanvas;
use crate::util::cf;

pub struct LineFlashEffect {

}

impl Effect for LineFlashEffect {
    fn exec(&mut self, target: &mut TargetCanvas, duration: f32) {
        let (w, h) = target.get_size();
        let fast = duration * 20.0;
        let line_state = (duration * 32.0 % 32.0) as usize;
        for y in 0..h {
            target.set(0 + line_state * 2, y, &cf(fast.sin() + fast.cos(), fast.sin(), fast.cos()), &None);
            target.set(63 - line_state * 2, y, &cf(1.0, 0.0, 0.0), &None);
        }
    }

    fn name(&self) -> &'static str {
        "Line flash"
    }
}