use std::fmt::{Display};
use chrono::{Local, TimeZone};
use fontdue::Font;
use crate::effects::effect::Effect;
use crate::target_canvas::TargetCanvas;
use crate::text::draw_text_center;
use crate::util::{c, time_to_new_year};

pub struct NewYearEffect {
    font: Font,
}

impl Effect for NewYearEffect {
    fn exec(&mut self, target: &mut TargetCanvas, duration: f32) {
        let duration = time_to_new_year();
        let hours = duration.num_hours();
        let minutes = duration.num_minutes() - duration.num_hours() * 60;
        let seconds = duration.num_seconds() - duration.num_minutes() * 60;
        let s = format!("{:02}:{:02}:{:02}", hours, minutes, seconds);
        draw_text_center(target, &self.font, s.as_str(), c(20, 255, 20), 1.0, 14);
    }

    fn name(&self) -> &'static str {
        "NEWYEAR"
    }
}

impl NewYearEffect {
    pub fn new() -> NewYearEffect {
        let font = include_bytes!("../../font.ttf") as &[u8];
        let font = Font::from_bytes(font, fontdue::FontSettings::default()).ok();
        NewYearEffect {
            font: font.unwrap(),
        }
    }
}