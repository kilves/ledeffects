use line_drawing::Bresenham;
use crate::pixel_color::PixelColor;
use crate::target_canvas::{TargetBlending, TargetCanvas};

pub fn draw_line(target: &mut TargetCanvas, x1: i32, y1: i32, x2: i32, y2: i32, color: &PixelColor, blending: &Option<TargetBlending>) {
    for (x, y) in Bresenham::new((x1, y1), (x2, y2)) {
        target.set(x as usize, y as usize, color, blending);
    }
}