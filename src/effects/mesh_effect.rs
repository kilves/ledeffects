use std::f32::consts::PI;
use std::fs::File;
use std::io;
use std::io::BufRead;
use std::path::{Path, PathBuf};
use cgmath::{Point3, Rad, Vector2, Vector3};
use fontdue::Font;
use image::io::Reader;
use log::warn;
use crate::effects::drawutils::draw_line;
use crate::effects::effect::Effect;
use crate::target_canvas::TargetCanvas;
use crate::text::draw_text_center;
use crate::util::{c, cf, mvp_matrix, ScaledTrig};

pub struct MeshEffect {
    vertices: Vec<Point3<f32>>,
    edges: Vec<Vector2<usize>>,
    font: Font,
}

impl Effect for MeshEffect {
    fn exec(&mut self, target: &mut TargetCanvas, duration: f32) {
        let mvp = mvp_matrix(Rad(duration.as_scaled_sin() * 1.66), Rad(duration / 2.0), duration.as_scaled_cos() - 0.5, 2.5);

        for edge in &self.edges {
            let point1 = self.vertices[edge[0]];
            let point2 = self.vertices[edge[1]];
            let (x1, y1) = target.project_3d(&point1, &mvp);
            let (x2, y2) = target.project_3d(&point2, &mvp);
            draw_line(target, x1, y1, x2, y2, &cf((duration * 5.5).scaled_sin(), (duration * 100.3).scaled_cos(), (duration * 1.3).scaled_cos()), &None);
        }
        let modulo = duration % 3.5;
        if modulo < 0.5 {
            draw_text_center(target, &self.font, "YOU", c(0xE5, 0, 0), 1.0, 2);
        } else if modulo < 1.0 {
            draw_text_center(target, &self.font, "LIKE", c(0xFF, 0x8D, 0), 1.0, 2);
        } else if modulo < 1.5 {
            draw_text_center(target, &self.font, "KISSING", c(0xFF, 0xEE, 0), 1.0, 2);
        } else if modulo < 2.0 {
            draw_text_center(target, &self.font, "BOYS", c(0x02, 0x81, 0), 1.0, 2);
        } else if modulo < 2.5 {
            draw_text_center(target, &self.font, "DONT", c(0x00, 0x4c, 0xff), 1.0, 2);
        } else if modulo < 3.0 {
            draw_text_center(target, &self.font, "YOU", c(0x77, 0x00, 0x88), 1.0, 2);
        }
    }

    fn name(&self) -> &'static str {
        "3D mesh"
    }
}

impl MeshEffect {
    pub fn new(path: &str) -> MeshEffect {
        let reader = Reader::open(path).unwrap();
        let lines = read_lines(path).unwrap();
        let mut vertices = vec![];
        let mut edges = vec![];
        let mut reading = false;
        for line in lines {
            if let Ok(l) = line {
                let data = l.split(" ").collect::<Vec<&str>>();
                if data[0] == "o" {
                    if reading {
                        break;
                    }
                    reading = true;
                    continue;
                }
                if data[0] == "v" {
                    vertices.push(Point3::new(
                        data[1].parse().unwrap(),
                        data[2].parse().unwrap(),
                        data[3].parse().unwrap()
                    ));
                    continue;
                }
                if data[0] == "l" {
                    edges.push(Vector2::new(
                        data[1].parse::<usize>().unwrap() - 1,
                        data[2].parse::<usize>().unwrap() - 1
                    ));
                    continue;
                }
                if data[0] != "#" && data[0] != "mtllib" {
                    break;
                }
            }
        }
        let font = include_bytes!("../../font.ttf") as &[u8];
        let font = Font::from_bytes(font, fontdue::FontSettings::default()).ok();
        MeshEffect {
            vertices, edges, font: font.unwrap()
        }
    }
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>> where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}