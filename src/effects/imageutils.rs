use std::cmp;
use image::RgbaImage;
use crate::image::LedImage;
use crate::target_canvas::{TargetCanvas, TargetBlending};
use crate::pixel_color::PixelColor;
use crate::util::ScaledTrig;

fn apply_contrast(c: u8, contrast: f32) -> u8 {
    let cf = c as f32;
    ((cf / 256.0).powf(contrast) * 256.0) as u8
}

// TODO ROTATION CODE VERY JANKY STUFF WITHOUT GOOD NAMES IM IN A HURRY

// Rotate a point clockwise to a given degree.
fn _rotate(p: (i32, i32), deg: f32) -> (i32, i32) {
    let radians:f32 = deg.to_radians();
    let px: f32 = p.0 as f32;
    let py: f32 = p.1 as f32;
    let cos = radians.cos();
    let sin = radians.sin();
    let x = ((px * cos) - (py * sin)).round();
    let y = ((px * sin) + (py * cos)).round();
    (x as i32, y as i32)
}

fn rotate(mut src: &RgbaImage, degree: i32, target: &mut TargetCanvas, duration: f32) {

    let w1 = 64;
    let h1 = 32;

    let degree = degree as f32; // convert to float

    // Using screen coords system, top left is always at (0,0)
    let mut min_x = 0;
    let mut max_x = 0;
    let mut min_y = 0;
    let mut max_y = 0;

    let top_right_1: (i32, i32) = (w1, 0);
    let top_right_2: (i32, i32) = _rotate(top_right_1, degree);
    min_x = cmp::min(min_x, top_right_2.0);
    max_x = cmp::max(max_x, top_right_2.0);
    min_y = cmp::min(min_y, top_right_2.1);
    max_y = cmp::max(max_y, top_right_2.1);

    let bottom_right_1: (i32, i32) = (w1, h1);
    let bottom_right_2: (i32, i32) = _rotate(bottom_right_1, degree);
    min_x = cmp::min(min_x, bottom_right_2.0);
    max_x = cmp::max(max_x, bottom_right_2.0);
    min_y = cmp::min(min_y, bottom_right_2.1);
    max_y = cmp::max(max_y, bottom_right_2.1);

    let bottom_left_1: (i32, i32) = (0, h1);
    let bottom_left_2: (i32, i32) = _rotate(bottom_left_1, degree);
    min_x = cmp::min(min_x, bottom_left_2.0);
    max_x = cmp::max(max_x, bottom_left_2.0);
    min_y = cmp::min(min_y, bottom_left_2.1);
    max_y = cmp::max(max_y, bottom_left_2.1);

    let w2 = ((min_x as f32).abs() + (max_x as f32).abs()) as i32 + 1;
    let h2 = ((min_y as f32).abs() + (max_y as f32).abs()) as i32 + 1;

    for (dest_y, y) in (0..).zip(min_y..max_y + 1) {
        for (dest_x, x) in (0..).zip(min_x..max_x + 1) {
            let point: (i32, i32) = _rotate((x,y), -degree);

            if point.0 >= 0 && point.0 < w1 && point.1 >=0 && point.1 < h1 {
                let p_index = ((point.1 * 64 + point.0) * 4) as usize;
                let data = src.to_vec();
                target.set((dest_x - ((duration * 3.0).scaled_cos() * 10.0 - 5.0) as i32).max(0) as usize, (dest_y - 10).max(0) as usize, &PixelColor {
                    red: (data[p_index] as f32 * (duration * 0.5).scaled_cos()) as u8,
                    green: (data[p_index + 1] as f32 * 0.0) as u8,
                    blue: (data[p_index + 2] as f32 * (duration * 56.0).scaled_sin()) as u8,
                }, &Some(TargetBlending::ALPHA(data[p_index + 3])));
            }
        }
    }
}

pub fn draw_rotated_image(target: &mut TargetCanvas, rgba_image: &RgbaImage, contrast: f32, deg: f32, duration: f32) {
    rotate(rgba_image, deg as i32, target, duration);
    /*
    for y in 0..h {
        for x in 0..w {
            let point = (y * w + x) * 4;
            target.set(x, y, &PixelColor {
                red: apply_contrast(rgba_image[point], contrast),
                green: apply_contrast(rgba_image[point + 1], contrast),
                blue: apply_contrast(rgba_image[point + 2], contrast),
            }, &Some(TargetBlending::ALPHA(rgba_image[point + 3])));
        }
    }
     */
}

pub fn draw_image(target: &mut TargetCanvas, rgba_image: &RgbaImage, contrast: f32, offset: (i32, i32)) {
    let (w, h) = target.get_size_i32();
    let x0 = w / 2 - (rgba_image.width() / 2) as i32;
    let y0 = h / 2 - (rgba_image.height() / 2) as i32;
    for y in 0..rgba_image.height() as i32 {
        for x in 0..rgba_image.width() as i32 {
            let pixel = rgba_image.get_pixel(x as u32, y as u32);
            target.set((x0 + x - offset.0) as usize, (y0 + y - offset.1) as usize, &PixelColor {
                red: apply_contrast(pixel.0[0], contrast),
                green: apply_contrast(pixel.0[1], contrast),
                blue: apply_contrast(pixel.0[2], contrast),
            }, &Some(TargetBlending::ALPHA(pixel.0[3])));
        }
    }
}