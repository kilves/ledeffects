use std::cmp::max;
use std::f32::consts::PI;
use fontdue::{Font, Metrics};
use image::RgbaImage;
use crate::effects::effect::Effect;
use crate::effects::imageutils::draw_image;
use crate::image::LedImage;
use crate::pixel_color::PixelColor;
use crate::target_canvas::{TargetBlending, TargetCanvas};
use crate::util::c;

pub struct QuadAnimationEffect {
    data: Vec<RgbaImage>,
    fps: usize,
    contrast: f32,
}

impl Effect for QuadAnimationEffect {
    fn exec(&mut self, target: &mut TargetCanvas, _duration: f32) {
        let frame = ((_duration * self.fps as f32) % self.data.len() as f32) as usize;
        let lol = (_duration * 1.0 + 0.0).sin() * 32.0 - 8.0;
        let lol2 = (_duration * 1.0 + 0.5).sin() * 32.0 - 8.0;
        let lol3 = (_duration * 1.0 + 1.0).sin() * 32.0 - 8.0;
        let lol4 = (_duration * 1.0 + 1.5).sin() * 32.0 - 8.0;
        draw_image(target, &self.data[frame], self.contrast, (lol as i32, 0));
        draw_image(target, &self.data[frame], self.contrast, (lol2 as i32, 0));
        draw_image(target, &self.data[frame], self.contrast, (lol3 as i32, 0));
        draw_image(target, &self.data[frame], self.contrast, (lol4 as i32, 0));
    }

    fn name(&self) -> &'static str {
        "Image effect"
    }
}

impl QuadAnimationEffect {
    pub fn new(data: Vec<RgbaImage>, fps: usize, contrast: f32) -> QuadAnimationEffect {
        QuadAnimationEffect {
            data, fps, contrast
        }
    }
}