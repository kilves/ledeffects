use crate::target_canvas::TargetCanvas;

pub trait Effect {
    fn exec(&mut self, target: &mut TargetCanvas, duration: f32);
    fn name(&self) -> &'static str;
}