use image::RgbaImage;
use crate::effects::effect::Effect;
use crate::target_canvas::{TargetCanvas, TargetBlending};
use crate::effects::imageutils::draw_image;
use crate::image::LedImage;
use crate::util::{cf, ScaledTrig};

pub struct BubiconEffect {
    fg_image: RgbaImage,
    bg_image: RgbaImage,
}

impl Effect for BubiconEffect {
    fn exec(&mut self, target: &mut TargetCanvas, duration: f32) {
        draw_image(target, &self.bg_image, 1.0, (0, 0));
        let (w, h) = target.get_size();
        let alpha = ((duration * 5.0).scaled_cos().scaled_sin() * 255.0) as u8;
        for x in 0..w {
            for y in 0..h {
                let sinval = (x as f32 / 3.0 + duration * 32.0).scaled_sin();
                target.set(x, y, &cf(0.2, 0.2 - sinval * 0.2, 0.1), &Some(TargetBlending::ALPHA(alpha)))
            }
        }
        draw_image(target, &self.fg_image, 1.0, (0, 0));
    }

    fn name(&self) -> &'static str {
        "Bubicon effect"
    }
}

impl BubiconEffect {
    pub fn new(fg_image: RgbaImage, bg_image: RgbaImage) -> BubiconEffect {
        BubiconEffect {
            fg_image,
            bg_image
        }
    }
}