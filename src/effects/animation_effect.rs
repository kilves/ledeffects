use std::cmp::max;
use fontdue::{Font, Metrics};
use image::RgbaImage;
use crate::effects::effect::Effect;
use crate::effects::imageutils::draw_image;
use crate::image::LedImage;
use crate::pixel_color::PixelColor;
use crate::target_canvas::{TargetBlending, TargetCanvas};
use crate::text::draw_text_center;
use crate::util::{c, cf, ScaledTrig};

pub struct AnimationEffect {
    frames: Vec<RgbaImage>,
    fps: usize,
    font: Option<Font>,
    text1: &'static str,
    text2: &'static str,
    contrast: f32,
    last_blinked: f32,
}

const BLINK_FREQUENCEY: f32 = 0.066;
impl Effect for AnimationEffect {
    fn exec(&mut self, target: &mut TargetCanvas, _duration: f32) {
        let blink_white = _duration > self.last_blinked + BLINK_FREQUENCEY;
        if self.frames.len() > 0 {
            let mut frame = ((_duration * self.fps as f32) % self.frames.len() as f32 * 2.0) as usize;
            if frame >= self.frames.len() {
                if frame == self.frames.len() * 2 {
                    frame = self.frames.len() * 2 - 1
                }
                frame = self.frames.len() - (frame - self.frames.len()) - 1;
            }
            draw_image(target, &self.frames[frame], self.contrast, (0, 0));
        }
        let brightness = (0.25 - (_duration % 0.5 - 0.25).abs()) * 4.0;
        let colorwhite = c(255, 255, 255);
        let wave = (_duration * 4.0).as_scaled_cos();
        let color = c(255,20,147).mix(&c(20, 255, 147), &wave);
        match &self.font {
            None => {}
            Some(font) => {
                if blink_white {
                    self.last_blinked = _duration;
                    draw_text_center(target, font, self.text2, colorwhite, 1.0, 2);
                } else {
                    draw_text_center(target, font, self.text2, color, 1.0, 2);
                }
            }
        }
    }

    fn name(&self) -> &'static str {
        "Image effect"
    }
}

impl AnimationEffect {
    pub fn new(data: Vec<RgbaImage>, fps: usize, text1: &'static str, text2: &'static str, contrast: f32) -> AnimationEffect {
        let font = include_bytes!("../../font.ttf") as &[u8];
        let font = Font::from_bytes(font, fontdue::FontSettings::default()).ok();
        AnimationEffect {
            frames: data, fps, font, text1, text2, contrast, last_blinked: 0.0,
        }
    }
}