use image::RgbaImage;
use crate::effects::effect::Effect;
use crate::target_canvas::TargetCanvas;
use crate::effects::imageutils::draw_image;
use crate::image::LedImage;

pub struct ImageEffect {
    pub image: RgbaImage
}

impl Effect for ImageEffect {
    fn exec(&mut self, target: &mut TargetCanvas, _duration: f32) {
        draw_image(target, &self.image, 1.0, (0, 0));
    }

    fn name(&self) -> &'static str {
        "Image effect"
    }
}