use fontdue::Font;
use crate::effects::effect::Effect;
use crate::effects::new_year_effect::NewYearEffect;
use crate::target_canvas::TargetCanvas;
use crate::text::draw_text_center;
use crate::util::c;

pub struct TextEffect {
    font: Font
}

impl Effect for TextEffect {
    fn exec(&mut self, target: &mut TargetCanvas, duration: f32) {
        draw_text_center(target, &self.font, "PIIRRÄ", c(255, 255, 255), 1.0, 20);
        draw_text_center(target, &self.font, "kilves.fi", c(255, 255, 255), 1.0, 10);
        draw_text_center(target, &self.font, "/draw", c(255, 255, 255), 1.0, 2);
    }

    fn name(&self) -> &'static str {
        "Text"
    }
}

impl TextEffect {
    pub fn new() -> TextEffect {
        let font = include_bytes!("../../font.ttf") as &[u8];
        let font = fontdue::Font::from_bytes(font, fontdue::FontSettings::default()).ok();
        TextEffect {
            font: font.unwrap(),
        }
    }
}