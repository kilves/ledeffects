use image::RgbaImage;
use rand::prelude::SmallRng;
use rand::{Rng, SeedableRng, thread_rng};
use rand::rngs::StdRng;
use rand::seq::SliceRandom;
use crate::effects::effect::Effect;
use crate::effects::imageutils::{draw_image, draw_rotated_image};
use crate::image::LedImage;
use crate::pixel_color::PixelColor;
use crate::target_canvas::{TargetBlending, TargetCanvas};
use crate::util::{c, cf};

pub struct BubiPressEffect {
    up: RgbaImage,
    down: RgbaImage,
    party_colors: Vec<PixelColor>
}

const BPM: f32 = 140.0;

impl Effect for BubiPressEffect {
    fn exec(&mut self, target: &mut TargetCanvas, duration: f32) {
        let (w, h) = target.get_size();
        let modulo = duration % (60.0 / BPM);
        let iteration = duration / (60.0 / BPM);
        let color_index = (iteration as usize) % self.party_colors.len();
        if modulo > 0.15 {
            draw_image(target, &self.up, 1.0, (0, 0));
        } else {
            draw_image(target, &self.down, 1.0, (0, 0));
        }
        let party_color = &self.party_colors[color_index];
        let brightness = 1.0 - modulo / (60.0 / BPM);
        let alpha = (96.0 * brightness) as u8;
        for x in 0..w {
            for y in 0..h {
                target.set(x, y, party_color, &Some(TargetBlending::ALPHA(alpha)));
            }
        }
    }

    fn name(&self) -> &'static str {
        "Bubicon 2022 effect"
    }
}

impl BubiPressEffect {
    pub fn new(up: RgbaImage, down: RgbaImage) -> BubiPressEffect {
        BubiPressEffect {
            up,
            down,
            party_colors: vec![
                c(246, 152, 53),
                c(221, 53, 92),
                c(18, 15, 82),
                c(142, 249, 61),
                c(221, 28, 221),
                c(255, 233, 37)
            ]
        }
    }
}