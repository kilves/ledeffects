use std::f32::consts::PI;
use crate::effects::effect::Effect;
use crate::target_canvas::TargetCanvas;
use crate::util::{ScaledTrig, cf};
use noise::Perlin;
use noise::NoiseFn;

pub struct PerlinEffect {
    octaves: Vec<(f64, f64)>
}

impl Effect for PerlinEffect {

    fn exec(&mut self, target: &mut TargetCanvas, duration: f32) {
        let perlin = Perlin::new();
        let scaled_duration = duration * 20.0;
        let size = target.get_size();
        for x in 0..size.0 {
            for y in 0..size.1 {
                let xf = x as f32 * 0.1;
                let yf = y as f32 * 0.1;
                let mut noiseval = 0.0f64;
                let max_noise = &self.octaves.iter().map(|o| o.1).sum();
                for octave in &self.octaves {
                    noiseval += perlin.get([
                        x as f64 * 0.03 * octave.0,
                        y as f64 * 0.03 * octave.0,
                        duration as f64 * 1.0
                    ]) * octave.1;
                }
                noiseval /= max_noise;
                noiseval += 0.15;
                target.set(x, y, &cf(
                    noiseval as f32 * (xf + yf + scaled_duration + PI / 2.0).scaled_cos(), // noiseval as f32,
                    noiseval as f32 * (xf + yf + scaled_duration + PI).scaled_cos(),//noiseval as f32 * (duration * 1.25).scaled_cos() * (y as f32).scaled_cos(),
                    noiseval as f32 * (xf + yf + scaled_duration + PI * 1.5).scaled_cos()//noiseval as f32 * (duration * 1.5).scaled_cos()
                ), &None);
            }
        }
    }

    fn name(&self) -> &'static str {
        "Perlin effect"
    }
}

impl PerlinEffect {
    pub fn new() -> PerlinEffect {
        PerlinEffect {
            octaves: vec![
                (4.0, 128.0),
                (8.0, 64.0),
                (16.0, 32.0),
                (32.0, 16.0),
                (64.0, 8.0),
                (128.0, 4.0)
            ]
        }
    }
}