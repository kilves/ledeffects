use image::RgbaImage;
use crate::effects::effect::Effect;
use crate::effects::imageutils::{draw_image, draw_rotated_image};
use crate::target_canvas::TargetCanvas;

pub struct Bubicon22Effect {
    data: RgbaImage
}

impl Effect for Bubicon22Effect {
    fn exec(&mut self, target: &mut TargetCanvas, duration: f32) {
        draw_rotated_image(target, &self.data, 1.0, duration as f32 * 200.0, duration * 10.5);
    }

    fn name(&self) -> &'static str {
        "Bubicon 2022 effect"
    }
}

impl Bubicon22Effect {
    pub fn new(data: RgbaImage) -> Bubicon22Effect {
        Bubicon22Effect {
            data,
        }
    }
}