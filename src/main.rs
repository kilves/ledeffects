mod renderers;
mod util;
mod effects;
mod target_canvas;
mod pixel_color;
mod pixel_render_target;
mod image;
mod text;

use std::{fs, thread};
use std::path::PathBuf;
use std::sync::mpsc;
use std::thread::sleep;
use std::time::{Duration, SystemTime};
use ::image::io::Reader;
use ::image::RgbaImage;
use cgmath::Vector2;
use noise::NoiseFn;
use rand::seq::SliceRandom;
use websocket::{ClientBuilder, Message, OwnedMessage};
use websocket::futures::{Future, Sink};
use crate::renderers::renderer::Renderer;
use crate::effects::perlin_effect::PerlinEffect;
use crate::effects::effects::{EffectConfig, Effects};

use crate::renderers::platform_renderer::PlatformRenderer;
use crate::effects::animation_effect::AnimationEffect;
use crate::effects::cube_effect::CubeEffect;
use crate::effects::effect::Effect;
use crate::effects::fireworks_effect::FireworksEffect;
use crate::effects::mesh_effect::MeshEffect;
use crate::effects::new_year_effect::NewYearEffect;
use crate::effects::quad_animation_effect::QuadAnimationEffect;
use crate::effects::text_effect::TextEffect;
use crate::util::{c, time_to_new_year};

fn load_images(folder: &str) -> Vec<RgbaImage> {
    let mut data = vec![];
    let mut paths: Vec<_> = fs::read_dir(folder)
        .unwrap()
        .into_iter()
        .filter(|img| img.is_ok())
        .map(|img| img.unwrap().path())
        .collect();
    paths.sort_by(|a, b| a.cmp(b));
    for path in paths {
        data.push(load_image(&path));
    }
    data
}

fn load_image(path: &PathBuf) -> RgbaImage {
    let reader = Reader::open(path).unwrap();
    reader.decode().unwrap().to_rgba()
}

fn main() {
    let mut renderer = PlatformRenderer::new(64, 32);
    let start = SystemTime::now();

    let mut effects = Effects::new(EffectConfig {
        interval: Duration::from_secs(30),
        fade: Duration::from_secs(1),
        size: renderer.get_size()
    });

    let mut client = ClientBuilder::new("wss://kilves.fi/ws")
        .unwrap()
        .connect(None);

    let (tx, rx) = mpsc::channel();
    thread::spawn(move || {
        if let Ok(mut client) = client {
            let message = Message::text("{\"led\": true}");
            client.send_message(&message).unwrap();
            for message in client.incoming_messages() {
                if let Ok(message) = message {
                    match message {
                        OwnedMessage::Text(text) => {
                            let res = json::parse(text.as_str());
                            if let Ok(json_value) = res {
                                tx.send(json_value).unwrap();
                            }
                        }
                        _ => {}
                    }
                }
            }
        }
    });


    // effects.put(XorEffect::new());
    // effects.put(LineFlashEffect {});
    // effects.put(SphereBlinkEffect::new());
    // effects.put(BlinkingEffect {});
    // effects.put(MetaEffect {});

    effects.put(QuadAnimationEffect::new(
        load_images("img/dance"), 60, 2.0
    ));
    effects.put(MeshEffect::new("img/boykisser.obj"));
    effects.put(AnimationEffect::new(
        load_images("img/peura"), 60, "", "", 2.0
    ));
    effects.put(AnimationEffect::new(
        load_images("img/walk2024"), 24, "2023", "2024", 2.0
    ));
    effects.put(CubeEffect::new());
    let mut newYearEffect = NewYearEffect::new();
    effects.put(newYearEffect);
    effects.put(FireworksEffect::new());

    effects.put(PerlinEffect::new());
    effects.put(TextEffect::new(

    ));

    /*
    effects.put(Bubicon22Effect::new(
        load_image("img/bubicon2022-2.png")
    ));
     */
    /*
    effects.put(BubiPressEffect::new(
        load_image("bubgif/pics/up.png"),
        load_image("bubgif/pics/down.png")
    ));
     */
    /*
    effects.put(BubiconEffect::new(
        load_image("img/soija.png"),
        load_image("img/bubicon.png")
    ));
     */

    /** Secondary timer init code. Scrapped for now because I didn't like it.
       let perlin = Perlin::new();
       let mut last_frame = SystemTime::now();
       let mut secondary_timer = SystemTime::now();
       let mut last_picked_speed = SystemTime::now()
           .duration_since(start)
           .unwrap()
           .as_secs_f32();
       let mut secondary_speedfactor = 1.0;
       let speeds: Vec<f32> = vec![1.8, 1.4, 1.2, 1.1, 1.0, 0.9, 0.8, 0.2, -0.8];
     */
    let mut previous_index = 0;
    let mut paused = 0.0;
    let mut is_paused = false;
    'running: loop {
        let now = SystemTime::now()
            .duration_since(start)
            .unwrap()
            .as_secs_f32();

        if now - paused > 6.0 {
            is_paused = false;
        }


        /** Secondary timer code. Scrapped for now because I didn't like it.
        let tick = SystemTime::now()
            .duration_since(last_frame)
            .unwrap();
        last_frame = SystemTime::now();

        if last_picked_speed + 274.4 < now {
            secondary_speedfactor = speeds.choose(&mut rand::thread_rng()).unwrap().clone();
            last_picked_speed = now;
        }
        if secondary_speedfactor < 0.0 {
            secondary_timer -= tick.mul_f32(secondary_speedfactor.abs());
        } else {
            secondary_timer += tick.mul_f32(secondary_speedfactor);
        }
        let mut secondary = 0.0;
        if secondary_timer > start {
            secondary = secondary_timer
                .duration_since(start)
                .unwrap()
                .as_secs_f32();
        } else {
            secondary = -(start
                .duration_since(secondary_timer)
                .unwrap()
                .as_secs_f32());
        }
        */


        if !is_paused {
            renderer.clear();
            let target = renderer.get_target();
            target.clear();
            if time_to_new_year().num_minutes() < 15 && time_to_new_year().num_seconds() > 0 {
                effects.override_newyear(target, now);
            } else {
                let index = effects.draw(target, now, now);
                previous_index = index as i32;
            }
        }

        if let Ok(json) = rx.try_recv() {
            if !json.has_key("pixels") {
                break;
            }
            let mut pixels = vec![];
            for (key, pixel) in json["pixels"].entries() {
                let split = key.split("-").collect::<Vec<&str>>();
                if split.len() != 2 {
                    break;
                }
                let x = split[0].parse::<i32>().unwrap();
                let y = split[1].parse::<i32>().unwrap();
                pixels.push(Vector2::new(x, y));
            }
            paused = now;
            is_paused = true;
            renderer.clear();
            renderer.swap(false).unwrap();
            renderer.clear();
            for pixel in &pixels {
                renderer.set(pixel.x, pixel.y, &c(255, 0, 0));
            }
            renderer.swap(false).unwrap();
            renderer.clear();
            for pixel in &pixels {
                renderer.set(pixel.x, pixel.y, &c(255, 0, 0));
            }
        }

        match renderer.swap(is_paused) {
            Err(()) => break 'running,
            Ok(()) => {}
        }
        sleep(Duration::new(0, 1_000_000_000u32 / 60));
    }
}
