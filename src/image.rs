pub struct LedImage {
    pub data: Vec<u8>,
    size: (usize, usize)
}