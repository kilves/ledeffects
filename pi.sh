#!/bin/bash

readonly RUSTFLAGS='-L lib'
readonly TARGET_HOST=pi@192.168.1.10
readonly TARGET_PATH=/home/pi/ledeffects
readonly TARGET_ARCH=armv7-unknown-linux-gnueabihf
readonly SOURCE_PATH=./target/${TARGET_ARCH}/release/ledeffects

RUSTFLAGS="-C target-feature=+crt-static -L lib" cross build --release --target=${TARGET_ARCH}
rsync ${SOURCE_PATH} ${TARGET_HOST}:${TARGET_PATH}
ssh -t ${TARGET_HOST} ${TARGET_PATH}